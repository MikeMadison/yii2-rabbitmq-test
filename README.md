Yii2 RabbitMQ extension example
-------------------------------
This is an basic RabbitMQ extension setup that you can use as an example to try or debug problems with extension within your application.
### Requirements
- Docker
- docker-compose

### Install
To make it up and running these shortcuts are provided:
```bash
make init_composer
make install
make publish
# In separate console tab
make consume
```
Enjoy!
